package ru.edu;

import liquibase.pro.packaged.A;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.edu.database.AdsRepositoryCRUD;
import ru.edu.model.Ad;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class ViewServletTest {

    public static final String PATH = "/WEB-INF/view.jsp";
    public static final String PATH_ID_NOT_FOUND = "index?error=id_incorrect";
    public static final String PATH_ID_IS_MISSING = "index?error=id_param_missing";

    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    AdsRepositoryCRUD crud = mock(AdsRepositoryCRUD.class);

    UUID id;
    Ad ad;
    ViewServlet servlet;

    @Before
    public void setUp() {
        try {
            Field instance = AdsRepositoryCRUD.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, crud);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void doGet() throws ServletException, IOException {
        ViewServlet servlet = new ViewServlet();
        UUID id = UUID.randomUUID();
        ad = new Ad();

        ad.setId(id);

        when(crud.selectById(id)).thenReturn(ad);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(request.getParameter("id")).thenReturn(id.toString());

        servlet.doGet(request, response);


        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, times(1)).setAttribute("ad", ad);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
        assertEquals(crud.selectById(id).getId().toString(), ad.getId().toString());
    }

    @Test
    public void doGetIdNull() throws ServletException, IOException {
        ViewServlet servlet = new ViewServlet();

        when(request.getParameter("id")).thenReturn(null);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(request, response);


        verify(response, times(1)).sendRedirect(PATH_ID_IS_MISSING);
        verify(response, never()).sendRedirect(PATH_ID_NOT_FOUND);
        verify(request, never()).getRequestDispatcher("/WEB-INF/view.jsp");
        verify(request, never()).setAttribute("ad", ad);

    }

    @Test
    public void doGetException() throws ServletException, IOException {
        ViewServlet servlet = new ViewServlet();


        when(request.getParameter("id")).thenReturn("somestring");
        when(crud.selectById(id)).thenReturn(null);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(request, response);


        verify(response, times(1)).sendRedirect(PATH_ID_NOT_FOUND);
        verify(response, never()).sendRedirect(PATH_ID_IS_MISSING);
        verify(request, never()).getRequestDispatcher("/WEB-INF/view.jsp");
        verify(request, never()).setAttribute("ad", ad);
    }

    @After
    public void resetSingleton() throws Exception {
        Field instance = AdsRepositoryCRUD.class.getDeclaredField("instance");
        instance.setAccessible(true);
        instance.set(null, null);
    }
}


