package ru.edu;

import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AuthFilterTest {

    HttpServletRequest httpReq = mock(HttpServletRequest.class);
    HttpServletResponse httpResp = mock(HttpServletResponse.class);
    FilterChain filterChain = mock(FilterChain.class);
    AuthFilter filter = new AuthFilter();


    @Test
    public void doFilterNullCookie() throws ServletException, IOException {

        when(httpReq.getCookies()).thenReturn(null);
        when(httpReq.getServletPath()).thenReturn("somestring");

        filter.doFilter(httpReq, httpResp, filterChain);

        verify(filterChain, times(1)).doFilter(httpReq, httpResp);
        verify(httpResp, never()).sendRedirect(anyString());
        verify(httpResp, never()).addCookie(any());
        verify(httpResp, never()).sendError(anyInt(), anyString());
    }

    @Test
    public void doFilterNullCookieEditPath() throws ServletException, IOException {

        when(httpReq.getCookies()).thenReturn(null);
        when(httpReq.getServletPath()).thenReturn("/edit");

        filter.doFilter(httpReq, httpResp, filterChain);

        verify(filterChain, never()).doFilter(httpReq, httpResp);
        verify(httpResp, never()).sendRedirect(anyString());
        verify(httpResp, never()).addCookie(any());
        verify(httpResp).sendError(anyInt(), anyString());
    }

    @Test
    public void doFilterCookieNotValid() throws ServletException, IOException {
        Cookie[] cookies = {new Cookie("some", "some"),
        new Cookie("some", "some")};

        when(httpReq.getCookies()).thenReturn(cookies);
        when(httpReq.getServletPath()).thenReturn("/edit");

        filter.doFilter(httpReq, httpResp, filterChain);

        verify(filterChain, never()).doFilter(httpReq, httpResp);
        verify(httpResp, never()).sendRedirect(anyString());
        verify(httpResp, never()).addCookie(any());
        verify(httpResp).sendError(anyInt(), anyString());
    }

    @Test
    public void doFilterAlreadyLogin() throws ServletException, IOException {
        Cookie[] cookies = {new Cookie("auth", "admin"),
                new Cookie("some", "some")};

        when(httpReq.getCookies()).thenReturn(cookies);
        when(httpReq.getParameter("auth")).thenReturn(null);
        when(httpReq.getServletPath()).thenReturn("/login");

        filter.doFilter(httpReq, httpResp, filterChain);

        verify(filterChain, never()).doFilter(httpReq, httpResp);
        verify(httpResp).sendRedirect("index?error=already_login");
        verify(httpResp, never()).sendRedirect("index?error=already_exit");
        verify(httpResp, times(1)).addCookie(any());
        verify(httpResp, never()).sendError(anyInt(), anyString());

    }

    @Test
    public void doFilterExitLogin() throws ServletException, IOException {
        Cookie[] cookies = {new Cookie("auth", "admin"),
                new Cookie("some", "some")};

        when(httpReq.getCookies()).thenReturn(cookies);
        when(httpReq.getParameter("auth")).thenReturn("exit");
        when(httpReq.getServletPath()).thenReturn("some");

        filter.doFilter(httpReq, httpResp, filterChain);

        verify(filterChain, never()).doFilter(httpReq, httpResp);
        verify(httpResp, never()).sendRedirect("index?error=already_login");
        verify(httpResp, times(1)).sendRedirect("index?error=already_exit");
        verify(httpResp, times(1)).addCookie(any());
        verify(httpResp, never()).sendError(anyInt(), anyString());

    }

    @Test
    public void doFilter() throws ServletException, IOException {
        Cookie[] cookies = {new Cookie("auth", "admin"),
                new Cookie("some", "some")};

        when(httpReq.getCookies()).thenReturn(cookies);
        when(httpReq.getParameter("auth")).thenReturn(null);
        when(httpReq.getServletPath()).thenReturn("some");


        filter.doFilter(httpReq, httpResp, filterChain);

        verify(filterChain).doFilter(httpReq, httpResp);
        verify(httpResp, never()).sendRedirect("index?error=already_login");
        verify(httpResp, never()).sendRedirect("index?error=already_exit");
        verify(httpResp, times(1)).addCookie(any());
        verify(httpResp, never()).sendError(anyInt(), anyString());

    }


}