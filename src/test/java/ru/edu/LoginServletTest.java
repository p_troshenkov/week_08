package ru.edu;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class LoginServletTest {
    public static final String PATH = "/WEB-INF/login.jsp";

    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);



    LoginServlet servlet = new LoginServlet();

    @Test
    public void doGet() throws ServletException, IOException {
        when(request.getParameter("error")).thenReturn(null);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(request, response);

        verify(request, never()).setAttribute("login", "incorrect_login");
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void doGetRandomError() throws ServletException, IOException {
        when(request.getParameter("error")).thenReturn("random_error");
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(request, response);

        verify(request, never()).setAttribute("login", "incorrect_login");
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void doGetError() throws ServletException, IOException {
        when(request.getParameter("error")).thenReturn("incorrect_login");
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(request, response);

        verify(request, times(1)).setAttribute("login", "incorrect_login");
        verify(dispatcher).forward(request, response);
    }


    @Test
    public void doPostDone() throws ServletException, IOException {

        when(request.getParameter("login")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("admin");

        servlet.doPost(request, response);

        verify(response, times(1)).sendRedirect("index?auth=done");
        verify(response, never()).sendRedirect("login?error=incorrect_login");
    }

    @Test
    public void doPostIncorrectLogin() throws ServletException, IOException {

        when(request.getParameter("login")).thenReturn("a");
        when(request.getParameter("password")).thenReturn("admin");

        servlet.doPost(request, response);

        verify(response, never()).sendRedirect("index?auth=done");
        verify(response, times(1)).sendRedirect("login?error=incorrect_login");

    }

    @Test
    public void doPostIncorrectPassword() throws ServletException, IOException {

        when(request.getParameter("login")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("a");

        servlet.doPost(request, response);

        verify(response, never()).sendRedirect("index?auth=done");
        verify(response, times(1)).sendRedirect("login?error=incorrect_login");

    }

}

