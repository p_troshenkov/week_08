package ru.edu;

import liquibase.pro.packaged.A;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.edu.database.AdsRepositoryCRUD;
import ru.edu.model.Ad;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class EditServletTest {

    public static final String PATH = "WEB-INF/edit.jsp";


    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    AdsRepositoryCRUD crud = mock(AdsRepositoryCRUD.class);

    UUID id;


    Ad ad;


    @Before
    public void setUp() {
        try {
            Field instance = AdsRepositoryCRUD.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, crud);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void doGet() throws ServletException, IOException {
        EditServlet servlet = new EditServlet();
        UUID id = UUID.randomUUID();
        ad = new Ad();
        ad.setId(id);

        when(crud.selectById(id)).thenReturn(ad);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(request.getParameter("id")).thenReturn(id.toString());

        servlet.doGet(request, response);


        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, times(1)).setAttribute("ad", ad);
        verify(response, times(1)).setCharacterEncoding("UTF-8");
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
        assertEquals(crud.selectById(id).getId().toString(), ad.getId().toString());
    }

    @Test
    public void doGetNullId() throws ServletException, IOException {
        EditServlet servlet = new EditServlet();

        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(request.getParameter("id")).thenReturn(null);

        servlet.doGet(request, response);


        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).setAttribute("ad", ad);
        verify(response, times(1)).setCharacterEncoding("UTF-8");
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
        
    }

    @Test
    public void doGetException() throws ServletException, IOException {
        EditServlet servlet = new EditServlet();

        when(crud.selectById(id)).thenReturn(null);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(request.getParameter("id")).thenReturn("somestring");

        servlet.doGet(request, response);


        verify(request, never()).getRequestDispatcher(PATH);
        verify(request, never()).setAttribute("ad", ad);
        verify(request, never()).getSession();
        verify(response).sendRedirect("index?error=id_incorrect");


    }

    @Test
    public void doPostEditAd() throws ServletException, IOException {
        EditServlet servlet = new EditServlet();

        UUID id = UUID.randomUUID();
        ad = new Ad();

        when(request.getParameter("ad_name")).thenReturn("ad_name");
        when(request.getParameter("ad_type")).thenReturn("ad_type");
        when(request.getParameter("price")).thenReturn("price");
        when(request.getParameter("message")).thenReturn("message");
        when(request.getParameter("user_name")).thenReturn("user_name");
        when(request.getParameter("user_email")).thenReturn("user_email");
        when(request.getParameter("user_phone")).thenReturn("user_phone");
        when(request.getParameter("url_image")).thenReturn("url_image");

        when(request.getParameter("id")).thenReturn(id.toString());
        when(crud.create(ad)).thenReturn(id);

        servlet.doPost(request, response);

        verify(response).sendRedirect("view?id=" + id);

    }



    @After
    public void resetSingleton() throws Exception {
        Field instance = AdsRepositoryCRUD.class.getDeclaredField("instance");
        instance.setAccessible(true);
        instance.set(null, null);
    }
}


