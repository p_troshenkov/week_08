package ru.edu.database;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import ru.edu.model.Ad;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class AdsRepositoryCRUDTest {
    private static final String INSERT = "INSERT INTO "
        + "ADVERTISEMENT (id, title, type, price, text, "
        + "publisher, email, phone, pictureUrl) "
        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE ADVERTISEMENT SET "
            + "title = ?, type = ?, price = ?, "
            + "text = ? ,publisher = ?, email = ?, phone = ?, pictureUrl = ? "
            + "WHERE id = ?";
    private static final String SELECT_ALL = "SELECT * FROM ADVERTISEMENT";
    private static final String SELECT =
            "SELECT * FROM ADVERTISEMENT WHERE id = ?";

    private static DataSource dataSourceMock = mock(DataSource.class);
    private static ThreadLocal<Connection> connectionMock = new ThreadLocal<>();

    @BeforeClass
    public static void setupJndi() throws NamingException, SQLException {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();
        ic.bind("java:/comp/env", ic);
        ic.bind("java:/comp/env/jdbc/adsDS", dataSourceMock);
        ic.bind("jdbc/adsDS", dataSourceMock);

        Context ctx = new InitialContext();
        Context env = (Context) ctx.lookup("java:/comp/env");
        DataSource dataSource = (DataSource) env.lookup("jdbc/adsDS");
        assertEquals(dataSourceMock, dataSource);
        when(dataSource.getConnection()).thenAnswer(invocationOnMock -> {return connectionMock.get();});
    }

    @Test
    public void insertAndUpdate() throws SQLException {
        Connection myConnectionMock = mock(Connection.class);
        connectionMock.set(myConnectionMock);
        AdsRepositoryCRUD instance = AdsRepositoryCRUD.getInstance();
        assertNotNull(instance);

        PreparedStatement preparedStatementInsert = mock(PreparedStatement.class);
        PreparedStatement preparedStatementUpdate = mock(PreparedStatement.class);
        when(myConnectionMock.prepareStatement(INSERT)).thenReturn(preparedStatementInsert);
        when(myConnectionMock.prepareStatement(UPDATE)).thenReturn(preparedStatementUpdate);
        when(preparedStatementInsert.executeUpdate()).thenReturn(1);
        when(preparedStatementUpdate.executeUpdate()).thenReturn(1);

        Ad ad = new Ad();

        ad.setTitle("ad_name");
        ad.setType("ad_type");
        ad.setPrice("price");
        ad.setText("message");
        ad.setPublisher("user_name");
        ad.setEmail("user_email");
        ad.setPhone("user_phone");
        ad.setPictureUrl("url_image");

        instance.create(ad);
        verify(preparedStatementInsert).setObject(1, ad.getId().toString());
        verify(preparedStatementInsert).setObject(2, "ad_name");
        verify(preparedStatementInsert).setObject(3, "ad_type");
        verify(preparedStatementInsert).setObject(4, "price");
        verify(preparedStatementInsert).setObject(5, "message");
        verify(preparedStatementInsert).setObject(6, "user_name");
        verify(preparedStatementInsert).setObject(7, "user_email");
        verify(preparedStatementInsert).setObject(8, "user_phone");
        verify(preparedStatementInsert).setObject(9, "url_image");

        ad.setTitle("ad_name1");
        instance.update(ad);

        verify(preparedStatementUpdate).setObject(9, ad.getId().toString());
        verify(preparedStatementUpdate).setObject(1, "ad_name1");
        verify(preparedStatementUpdate).setObject(2, "ad_type");
        verify(preparedStatementUpdate).setObject(3, "price");
        verify(preparedStatementUpdate).setObject(4, "message");
        verify(preparedStatementUpdate).setObject(5, "user_name");
        verify(preparedStatementUpdate).setObject(6, "user_email");
        verify(preparedStatementUpdate).setObject(7, "user_phone");
        verify(preparedStatementUpdate).setObject(8, "url_image");

    }

    @Test
    public void selectAndSelectAll() throws SQLException {
        Connection myConnectionMock = mock(Connection.class);
        connectionMock.set(myConnectionMock);
        AdsRepositoryCRUD instance = AdsRepositoryCRUD.getInstance();
        assertNotNull(instance);

        PreparedStatement preparedStatementSelect = mock(PreparedStatement.class);
        PreparedStatement preparedStatementSelectAll = mock(PreparedStatement.class);

        ResultSet resultSetSelectAll = mock(ResultSet.class);
        ResultSet resultSetSelect = mock(ResultSet.class);

        when(myConnectionMock.prepareStatement(SELECT)).thenReturn(preparedStatementSelect);
        when(myConnectionMock.prepareStatement(SELECT_ALL)).thenReturn(preparedStatementSelectAll);
        when(preparedStatementSelectAll.executeQuery()).thenReturn(resultSetSelectAll);
        when(preparedStatementSelect.executeQuery()).thenReturn(resultSetSelect);

        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();

        when(resultSetSelectAll.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSetSelectAll.getString("id")).thenReturn(id1.toString()).thenReturn(id2.toString());
        when(resultSetSelectAll.getString("title")).thenReturn("title1").thenReturn("title2");
        when(resultSetSelectAll.getString("type")).thenReturn("type1").thenReturn("type2");
        when(resultSetSelectAll.getString("price")).thenReturn("price1").thenReturn("price2");
        when(resultSetSelectAll.getString("text")).thenReturn("text1").thenReturn("text2");
        when(resultSetSelectAll.getString("publisher")).thenReturn("publisher1").thenReturn("publisher2");
        when(resultSetSelectAll.getString("email")).thenReturn("email1").thenReturn("email2");
        when(resultSetSelectAll.getString("phone")).thenReturn("phone1").thenReturn("phone2");
        when(resultSetSelectAll.getString("pictureUrl")).thenReturn("pictureUrl1").thenReturn("pictureUrl2");

        when(resultSetSelect.next()).thenReturn(true).thenReturn(false);
        when(resultSetSelect.getString("id")).thenReturn(id1.toString());
        when(resultSetSelect.getString("title")).thenReturn("title1");
        when(resultSetSelect.getString("type")).thenReturn("type1");
        when(resultSetSelect.getString("price")).thenReturn("price1");
        when(resultSetSelect.getString("text")).thenReturn("text1");
        when(resultSetSelect.getString("publisher")).thenReturn("publisher1");
        when(resultSetSelect.getString("email")).thenReturn("email1");
        when(resultSetSelect.getString("phone")).thenReturn("phone1");
        when(resultSetSelect.getString("pictureUrl")).thenReturn("pictureUrl1");

        List<Ad> selectAllList = instance.selectAll();
        Ad ad1 = selectAllList.get(0);
        Ad ad2 = selectAllList.get(1);
        assertEquals(id1.toString(), ad1.getId().toString());
        assertEquals(id2.toString(), ad2.getId().toString());

        assertEquals("text1", ad1.getText());
        assertEquals("email2", ad2.getEmail());


        Ad selectAd = instance.selectById(id1);

        assertEquals(id1.toString(), selectAd.getId().toString());

        assertEquals("text1", selectAd.getText());
        assertEquals("email1", selectAd.getEmail());
    }
}