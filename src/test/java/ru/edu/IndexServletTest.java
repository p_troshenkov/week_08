package ru.edu;

import liquibase.pro.packaged.A;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.edu.database.AdsRepositoryCRUD;
import ru.edu.model.Ad;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class IndexServletTest {

    public static final String PATH = "/WEB-INF/index.jsp";

    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    AdsRepositoryCRUD crud = mock(AdsRepositoryCRUD.class);

    @Before
    public void setUp() {
        try {
            Field instance = AdsRepositoryCRUD.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, crud);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void doGet() throws ServletException, IOException {
        IndexServlet servlet = new IndexServlet();
        Ad ad = new Ad();
        Ad ad1 = new Ad();

        UUID id = UUID.randomUUID();
        UUID id1 = UUID.randomUUID();

        ad.setId(id);
        ad1.setId(id1);

        List<Ad> adList = new ArrayList<>();
        adList.add(ad);
        adList.add(ad1);

        when(crud.selectAll()).thenReturn(adList);
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(request, response);

        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, times(1)).setAttribute("adList", adList);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
        assertEquals(crud.selectAll().get(0).getId().toString(), ad.getId().toString());
        assertEquals(crud.selectAll().get(1).getId().toString(), ad1.getId().toString());
    }

    @After
    public void resetSingleton() throws Exception {
        Field instance = AdsRepositoryCRUD.class.getDeclaredField("instance");
        instance.setAccessible(true);
        instance.set(null, null);
    }
}
