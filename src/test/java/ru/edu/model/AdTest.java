package ru.edu.model;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class AdTest {
    Ad ad = new Ad();



    @Test
    public void setId() {
        UUID id = UUID.randomUUID();
        ad.setId(id);
        assertEquals(id.toString(), ad.getId().toString());
    }

    @Test
    public void setTitle() {
        ad.setTitle("title1");
        assertEquals("title1", ad.getTitle());
    }

    @Test
    public void setType() {
        ad.setType("type1");
        assertEquals("type1", ad.getType());
    }

    @Test
    public void setPrice() {
        ad.setPrice("price1");
        assertEquals("price1", ad.getPrice());
    }

    @Test
    public void setText() {
        ad.setText("text1");
        assertEquals("text1", ad.getText());
    }

    @Test
    public void setPublisher() {
        ad.setPublisher("publisher1");
        assertEquals("publisher1", ad.getPublisher());
    }

    @Test
    public void setEmail() {
        ad.setEmail("email1");
        assertEquals("email1", ad.getEmail());
    }

    @Test
    public void setPhone() {
        ad.setPhone("phone1");
        assertEquals("phone1", ad.getPhone());
    }

    @Test
    public void setPictureUrl() {
        ad.setPictureUrl("pictureUrl1");
        assertEquals("pictureUrl1", ad.getPictureUrl());
    }
}
