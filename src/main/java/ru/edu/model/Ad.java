package ru.edu.model;


import java.util.UUID;

public class Ad {
    /**
     * UUID.
     */
    private UUID id;
    /**
     * Заголовок объявления.
     */
    private String title;
    /**
     * Тип объявления.
     */
    private String type;
    /**
     * Заявленная цена.
     */
    private String price;
    /**
     * Техт объявления.
     */
    private String text;
    /**
     * Автор объявления.
     */
    private String publisher;
    /**
     * Емэйл автора.
     */
    private String email;
    /**
     * Телефон автора.
     */
    private String phone;
    /**
     * Адрес изображения.
     */
    private String pictureUrl;

    /**
     * Конструктор по умолчанию.
     */
    public Ad() {
    }

    /**
     * Сеттер id.
     * @param uuid
     */
    public void setId(final UUID uuid) {
        id = uuid;
    }

    /**
     * Геттер id.
     * @return UUID
     */
    public UUID getId() {
        return id;
    }

    /**
     * Сеттер заголовка.
     * @param adTitle
     */
    public void setTitle(final String adTitle) {
        title = adTitle;
    }

    /**
     * Геттер заголовка.
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Сеттер типа.
     * @param adType
     */
    public void setType(final String adType) {
        type = adType;
    }

    /**
     * Геттер типа.
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * Сеттер стоимости.
     * @param adPrice
     */
    public void setPrice(final String adPrice) {
        price = adPrice;
    }

    /**
     * Геттер стоимости.
     * @return String
     */
    public String getPrice() {
        return price;
    }

    /**
     * Сеттер текста.
     * @param adText
     */
    public void setText(final String adText) {
        text = adText;
    }

    /**
     * Геттер текста.
     * @return String
     */
    public String getText() {
        return text;
    }

    /**
     * Сеттер автора.
     * @param adPublisher
     */
    public void setPublisher(final String adPublisher) {
        publisher = adPublisher;
    }

    /**
     * Геттер автора.
     * @return String
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Сеттер емэйла автора.
     * @param adEmail
     */
    public void setEmail(final String adEmail) {
        email = adEmail;
    }

    /**
     * Геттер емэйла автора.
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Сеттер номера телефона.
     * @param adPhone
     */
    public void setPhone(final String adPhone) {
        phone = adPhone;
    }

    /**
     * Геттер номера телефона.
     * @return String
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Сеттер урл изображения.
     * @param adPictureUrl
     */
    public void setPictureUrl(final String adPictureUrl) {
        pictureUrl = adPictureUrl;
    }

    /**
     * Геттер урл изображения.
     * @return String
     */
    public String getPictureUrl() {
        return pictureUrl;
    }
}
