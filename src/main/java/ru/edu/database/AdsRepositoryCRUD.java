package ru.edu.database;

import ru.edu.model.Ad;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AdsRepositoryCRUD {
    /**
     * Синглтон CRUD.
     */
    private static AdsRepositoryCRUD instance;

    /**
     * Источник данных.
     */
    private DataSource dataSource;

    /**
     * Шаблон запроса заполнения таблицы.
     */
    private static final String INSERT = "INSERT INTO "
            + "ADVERTISEMENT (id, title, type, price, text, "
            + "publisher, email, phone, pictureUrl) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Шаблон запроса изменения данных таблицы.
     */
    private static final String UPDATE = "UPDATE ADVERTISEMENT SET "
            + "title = ?, type = ?, price = ?, "
            + "text = ? ,publisher = ?, email = ?, phone = ?, pictureUrl = ? "
            + "WHERE id = ?";

    /**
     * Шаблон запроса отображения всех данных в таблице.
     */
    private static final String SELECT_ALL = "SELECT * FROM ADVERTISEMENT";

    /**
     * Шаблон запроса отображения данных по id.
     */
    private static final String SELECT =
            "SELECT * FROM ADVERTISEMENT WHERE id = ?";

    /**
     * Конструктор класса.
     *
     * @param ds
     */
    public AdsRepositoryCRUD(final DataSource ds) {
        dataSource = ds;
    }

    /**
     * Синглтон CRUD для доступа к бд.
     * @return CRUD
     */
    public static AdsRepositoryCRUD getInstance() {
        synchronized (AdsRepositoryCRUD.class) {
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env = (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource = (DataSource) env
                            .lookup("jdbc/adsDS");
                    instance = new AdsRepositoryCRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param ad
     * @return сгенерированный UUID
     */

    public UUID create(final Ad ad) {
        UUID id = UUID.randomUUID();
        ad.setId(id);
        int insertedRows = execute(
                INSERT,
                id.toString(),
                ad.getTitle(),
                ad.getType(),
                ad.getPrice(),
                ad.getText(),
                ad.getPublisher(),
                ad.getEmail(),
                ad.getPhone(),
                ad.getPictureUrl()
        );

        if (insertedRows == 0) {
            throw new IllegalStateException("Not inserted");
        }

        return id;
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return Ad
     */
    public Ad selectById(final UUID id) {
        return query(SELECT, id.toString()).get(0);
    }

    /**
     * Получение всех записей из БД.
     *
     * @return List<Ad>
     */

    public List<Ad> selectAll() {
        return query(SELECT_ALL);
    }

    /**
     * Обновление записи в БД.
     *
     * @param ad
     * @return количество обновленных записей
     */
    public int update(final Ad ad) {
        int updatedRows = execute(
                UPDATE,
                ad.getTitle(),
                ad.getType(),
                ad.getPrice(),
                ad.getText(),
                ad.getPublisher(),
                ad.getEmail(),
                ad.getPhone(),
                ad.getPictureUrl(),
                ad.getId().toString()
        );
        if (updatedRows == 0) {
            throw new IllegalStateException("Not updated");
        }
        return updatedRows;
    }

    /**
     * Создание Update запроса.
     * @param sql
     * @param values
     * @return количество обработанных строк
     */
    private int execute(final String sql, final Object... values) {
        try (PreparedStatement statement = dataSource
                .getConnection()
                .prepareStatement(sql)
        ) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Создание Query запроса.
     * @param sql
     * @param values
     * @return список студентов
     */
    private List<Ad> query(final String sql, final Object... values) {
        try (PreparedStatement statement = dataSource
                .getConnection()
                .prepareStatement(sql)
        ) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Ad> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Сборка объекта Student из БД.
     * @param resultSet
     * @return объект Student
     * @throws SQLException
     */
    private Ad map(final ResultSet resultSet) throws SQLException {
        Ad ad = new Ad();
        ad.setId(UUID.fromString(resultSet.getString("id")));
        ad.setTitle(resultSet.getString("title"));
        ad.setType(resultSet.getString("type"));
        ad.setPrice(resultSet.getString("price"));
        ad.setText(resultSet.getString("text"));
        ad.setPublisher(resultSet.getString("publisher"));
        ad.setEmail(resultSet.getString("email"));
        ad.setPhone(resultSet.getString("phone"));
        ad.setPictureUrl(resultSet.getString("pictureUrl"));
        return ad;
    }


}
