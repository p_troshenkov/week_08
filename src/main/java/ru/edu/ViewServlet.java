package ru.edu;

import ru.edu.database.AdsRepositoryCRUD;
import ru.edu.model.Ad;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class ViewServlet extends HttpServlet {
    /**
     * CRUD.
     */
    private AdsRepositoryCRUD crud = AdsRepositoryCRUD.getInstance();

    /**
     * @see HttpServlet
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        if (req.getParameter("id") == null) {
            resp.sendRedirect("index?error=id_param_missing");
            return;
        }

        try {
            UUID id = UUID.fromString(req.getParameter("id"));
            Ad ad = crud.selectById(id);
            req.setAttribute("ad", ad);
        } catch (IllegalArgumentException
                | IndexOutOfBoundsException exception) {
            resp.sendRedirect("index?error=id_incorrect");
            return;
        }

        req.getRequestDispatcher("/WEB-INF/view.jsp").forward(req, resp);
    }

}
