package ru.edu;


import ru.edu.database.AdsRepositoryCRUD;
import ru.edu.model.Ad;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class EditServlet extends HttpServlet {
    /**
     * CRUD.
     */
    private AdsRepositoryCRUD crud = AdsRepositoryCRUD.getInstance();

    /**
     * @see HttpServlet
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        String idParameter = req.getParameter("id");

        if (idParameter != null) {
            try {
                UUID id = UUID.fromString(idParameter);
                Ad ad = crud.selectById(id);
                req.setAttribute("ad", ad);
            } catch (IllegalArgumentException
                    | IndexOutOfBoundsException exception) {
                resp.sendRedirect("index?error=id_incorrect");
                return;
            }
        }
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("WEB-INF/edit.jsp").forward(req, resp);
    }

    /**
     * @see HttpServlet
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {

        Ad ad = new Ad();

        ad.setTitle(req.getParameter("ad_name"));
        ad.setType(req.getParameter("ad_type"));
        ad.setPrice(req.getParameter("price"));
        ad.setText(req.getParameter("message"));
        ad.setPublisher(req.getParameter("user_name"));
        ad.setEmail(req.getParameter("user_email"));
        ad.setPhone(req.getParameter("user_phone"));
        ad.setPictureUrl(req.getParameter("url_image"));


        String idParameter = req.getParameter("id");

        if (idParameter != null) {
            ad.setId(UUID.fromString(idParameter));
            crud.update(ad);
        } else {
            UUID id = crud.create(ad);
        }

        resp.sendRedirect("view?id=" + ad.getId().toString());
    }
}
