package ru.edu;

import ru.edu.database.AdsRepositoryCRUD;
import ru.edu.model.Ad;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class IndexServlet extends HttpServlet {
    /**
     * CRUD.
     */
    private AdsRepositoryCRUD crud = AdsRepositoryCRUD.getInstance();

    /**
     * @see HttpServlet
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {

        List<Ad> adList = crud.selectAll();

        req.setAttribute("adList", adList);

        req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
    }
}
