package ru.edu;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

public class AuthFilter implements Filter {
    /**
     * Ошибка 403.
     */
    public static final int NOT_AUTHORIZED_ERROR = 403;

    /**
     * Время жизни cookie.
     */
    public static final int COOKIE_MAX_AGE = 60;

    /**
     * Время жизни для удаления cookie.
     */
    public static final int COOKIE_DELETE_AGE = 0;

    /**
     * @see Filter
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {

    }

    /**
     * @see Filter
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(
            final ServletRequest servletRequest,
            final ServletResponse servletResponse,
            final FilterChain filterChain
    ) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResp = (HttpServletResponse) servletResponse;
        Cookie[] cookies = httpReq.getCookies();

        if (cookies != null && cookieIsValid(
                httpReq,
                httpResp,
                filterChain,
                cookies
        )) {
            Cookie cookie = new Cookie("auth", "admin");
            cookie.setMaxAge(COOKIE_MAX_AGE);
            cookie.setPath("/");


            String authParam = httpReq.getParameter("auth");
            if (authParam != null && authParam.equals("exit")) {
                    cookie.setMaxAge(COOKIE_DELETE_AGE);
                }

            httpResp.addCookie(cookie);
            servletRequest.setAttribute("auth", "admin");

            if (httpReq.getServletPath().contains("/login")) {
                httpResp.sendRedirect("index?error=already_login");
            } else if (authParam != null && authParam.equals("exit")) {
                httpResp.sendRedirect("index?error=already_exit");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }

        } else {
            if (httpReq.getServletPath().contains("/edit")) {
                httpResp.setCharacterEncoding("UTF-8");
                httpResp.sendError(NOT_AUTHORIZED_ERROR, "Not authorized");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
    }

    /**
     * Проверка кук.
     * @param request
     * @param response
     * @param chain
     * @param cookies
     * @return boolean
     * @throws IOException
     * @throws ServletException
     */

    private boolean cookieIsValid(
            final ServletRequest request,
            final ServletResponse response,
            final FilterChain chain,
            final Cookie[] cookies
    ) throws IOException, ServletException {
        Optional<Cookie> authCookie = Arrays.stream(cookies)
                .filter(cookie -> "auth".equals(cookie.getName()))
                .findFirst();
        return authCookie.isPresent();

    }

    /**
     * @see Filter
     */
    @Override
    public void destroy() {

    }
}
