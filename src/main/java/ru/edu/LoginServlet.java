package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet  extends HttpServlet {
    /**
     * @see HttpServlet
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        String error = req.getParameter("error");
        if (error != null && error.equals("incorrect_login")) {
            req.setAttribute("login", "incorrect_login");
        }
        req.getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

    /**
     * @see HttpServlet
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (login.equals("admin") && password.equals("admin")) {
            Cookie cookie = new Cookie("auth", "admin");
            cookie.setMaxAge(AuthFilter.COOKIE_MAX_AGE);
            cookie.setPath("/");
            resp.addCookie(cookie);
            resp.sendRedirect("index?auth=done");
        } else {
            resp.sendRedirect("login?error=incorrect_login");
        }

    }
}
