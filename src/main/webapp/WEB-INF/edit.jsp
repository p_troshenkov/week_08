﻿<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta charset="UTF-8" />
  <title>Редактирование объявления</title>
  <link href="style.css" rel="stylesheet" type="text/css" />
 </head>
 <body>
  <div class="layout">
    <c:import url="/WEB-INF/templates/login.jsp"/>
    <div class="logo"><h1>Редактирование объявления</h1></div>
    <div class="content">
        <form class="edit_form" name="edit" method="POST" action="">
          <div>Заголовок</div>
          <input type="text" name="ad_name" required="required" value="<c:out value = "${ad.title}" />">

          <div class="infofield">Тип объявления</div>
          <select name="ad_type" required="required" value="<c:out value = "${ad.type}" />">
            <option value="service">Услуга</option>
            <option value="buy">Покупка</option>
            <option value="sell">Продажа</option>
          </select>

          <div>Текст объявления</div>
          <textarea name="message" ><c:out value = "${ad.text}" /></textarea>

          <div>Ваше имя</div>
          <input type="text" name="user_name" required="required" value="<c:out value = "${ad.publisher}" />">

          <div>Ваш email</div>
          <input type="email" name="user_email" required="required" value="<c:out value = "${ad.email}" />">

          <div>Ваш телефон</div>
          <input type="tel" name="user_phone" required="required" value="<c:out value = "${ad.phone}" />">

          <div>URL изображения</div>
          <input type="url" name="url_image" value="<c:out value = "${ad.pictureUrl}" />">

          <div>Цена</div>
          <input type="text" name="price" required="required" value="<c:out value = "${ad.price}" />">

          <input type="submit" name="submit_btn" value=" Сохранить ">
        </form>
    </div>
    <div class="footer">
    <a href="index">Доска объявлений</a>
    |
    <a href="about">Об авторе</a>
    </div>

  </div>
 </body>
</html>