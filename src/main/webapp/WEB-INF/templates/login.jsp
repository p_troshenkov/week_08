<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${cookie.auth.value == 'admin'}">
    <div class="auth">
        <p><input type="submit" value=" Выйти из аккаунта "
        onclick="window.location='index?auth=exit'" />
        <br/>
	</div>
	
    <div class="login">
		Вы вошли как admin
	</div>
</c:if>

<c:if test="${cookie.auth.value != 'admin'}">
    <div class="auth">
		<p><input type="submit" value="Войти в аккаунт "
        onclick="window.location='login';" />
        <br/></div>
    <div class="login">
		Вы вошли как гость
	</div>
</c:if>
