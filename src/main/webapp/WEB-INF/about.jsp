﻿<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta charset="UTF-8" />
  <title>Об авторе</title>
  <link href="./style.css" rel="stylesheet" type="text/css" />
</head>
 <body>
  <div class="layout">
    <c:import url="/WEB-INF/templates/login.jsp"/>
    <div class="logo"><h1>Об авторе</h1></div>
    <div class="content">
        <h2>
        Петр Трошенков<br><br><a href="https://bitbucket.org/p_troshenkov/week_08/src/master/">Ссылка на проект BitBucket</a>
        </h2>
    </div>
    <div class="footer">
    <a href="index">Доска объявлений</a>
    |
    <a href="about">Об авторе</a>
    </div>

  </div>
 </body>
</html>