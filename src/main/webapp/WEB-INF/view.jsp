﻿<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta charset="UTF-8" />
  <title>Просмотр объявления</title>
  <link href="style.css" rel="stylesheet" type="text/css" />
 </head>
 <body>
  <div class="layout">
    <c:import url="/WEB-INF/templates/login.jsp"/>
    <div class="logo"><h1>Доска объявлений</h1></div>
    <div class="content">
                    <div class ="ad">
                        <div class = "image"><img src="<c:out value = "${ad.pictureUrl}"/>"
                                               width="196" height="196"></div>
                        <div class = "cont">
                            <div class = "title"><c:out value = "${ad.title}" /> </div>
                            <div class = "type"> <c:out value = "${ad.type}" /></div>
                            <div class = "text"> <c:out value = "${ad.text}" /></div>
                            <div class = "other">
                                <div class = "price"><c:out value = "${ad.price}"/> </div>
                                <div class = "links">
                                    <c:if test="${cookie.auth.value == 'admin'}">
                                    <a href="edit?id=<c:out value = "${ad.id}"/>">Редактирование</a>
                                    </c:if>

                                </div>
                           </div>
                        </div>
    				</div>

    </div>
    <div class="footer">
        <a href="index">Доска объявлений</a>
        |
        <a href="about">Об авторе</a>
        </div>

  </div>
 </body>
</html>