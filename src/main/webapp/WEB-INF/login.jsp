﻿<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta charset="UTF-8" />
  <title>Страница регистрации</title>
  <link href="style.css" rel="stylesheet" type="text/css" />
 </head>
 <body>
  <div class="layout">

    <div class="logo"><h1>Страница регистрации</h1></div>
    <div class="content">
        <form class="login_form" name="login_form" method="POST" action="">
          <div>Имя пользователя</div>
          <input type="text" name="login" required="required" >
          <div>Пароль</div>
          <input type="text" name="password" required="required" >
            <div></div>
          <input type="submit" name="submit_btn" value=" Войти ">
        </form>
        <c:if test="${param.error == 'incorrect_login'}">
                    <p>Неверный логин / пароль<p>
        </c:if>
    </div>
    <div class="footer">
    <a href="index">Доска объявлений</a>
    |
    <a href="about">Об авторе</a>
    </div>

  </div>
 </body>
</html>